#include <stdio.h>

int main()
{
    char palabra[20];
	char p;    / Puntero de la cadena */
	int contar=0;

    printf("Ingrese una Palabra:\n"); scanf("%s",palabra);

	p = palabra;  
	while (*p != '\0')   
	{
		contar++;
		printf( "%i -", contar );
		printf( "%c\n", p );	/ Mostramos la letra actual */
		p++;			
	}
	printf( "La Palabra \"%s\" Tiene %i Letras.\n", palabra, contar );
    
}
